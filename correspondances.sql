SELECT *
  INTO CSV("table-appartenance-geo-communes-18.csv", {headers:true, separator:","})
  FROM XLS("table-appartenance-geo-communes-18_V2.xls", {headers:true, sheetid: "COM", range: "A5:P99999"});
