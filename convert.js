'use strict';

const alasql = require('alasql');
const fs = require('fs');
const {promisify} = require('util');
const {join} = require('path');
const readFile = promisify(fs.readFile);

readFile(join(__dirname, 'correspondances.sql'), {encoding: 'utf8'})
  .then(sql => alasql(sql))
  .then(() => {
    console.log('👍');
    process.exit(0);
  })
  .catch((error) => {
    console.error(error.message);
    process.exit(1);
  })
